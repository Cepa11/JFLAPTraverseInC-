#include "../Header/Automata.h"

const string Automata::numeral = "123456789";
const string Automata::numeralConCero = "0123456789";
const string Automata::cero = "0";
const string Automata::guion = "-";

Automata::Automata(){
    this->exitosos = 0;
    this->fallidos = 0;
}

void Automata::viajaPorAutomata(string hileraDeEntrada, int idEstadoActual){
    //cout << "Actualmente me encuentro en el estado " << nombreEstadoActual <<endl;   

    if(!hileraDeEntrada.empty()){
        //cout << "Hilera de entrada: " << hileraDeEntrada << endl;
        char caracterActual = hileraDeEntrada.at(0);
    
        string nuevaHilera = hileraDeEntrada.substr(1, hileraDeEntrada.size()-1);

        Estado * existe = buscaEnLista(idEstadoActual);
        if(existe->obtieneID()==-1){
            this->incrementaFallidos();
            return;
        }

        vector<int> resultados = existe->buscaPosiblesDestinos(caracterActual);
        vector<int>::iterator iterador;
        int nombreEstadoEncontrado;
        Estado * estadoEncontrado = new Estado();
        if(resultados.empty()){
            this->incrementaFallidos();
            return;
        }
        for(iterador=resultados.begin();
        iterador!=resultados.end();
        iterador++){
            nombreEstadoEncontrado = *iterador;
            estadoEncontrado = buscaEnLista(nombreEstadoEncontrado);
            if(estadoEncontrado->obtieneID()!=-1){
                viajaPorAutomata(nuevaHilera, estadoEncontrado->obtieneID());
            }
        }
    }
    if(hileraDeEntrada.empty()){
        Estado * estadoFinal = buscaEnLista(idEstadoActual);
        if(estadoFinal->obtieneEsFinal()){
            this->incrementaExitosos();
            return;
        }
        this->incrementaFallidos();
        return;
    }
}

Estado * Automata::buscaEnLista(int id){
    Estado * temp = new Estado();;
    int tamanoArreglo = todosEstados->size();
    if(id > tamanoArreglo){
        return temp;
    }
    return todosEstados->at(id);
}

bool Automata::contieneLista(int aBuscar, vector<int> * fuente){
    vector<int>::iterator iterador;
    for(iterador = fuente->begin(); iterador != fuente->end(); iterador++){
        if(*iterador==aBuscar){
            return true;
        }
    }
    return false;
}
bool Automata::leeYRecorreAutomata(ManipulaArchivos &manipulaArchivos, char nombreArchivo[]){
    string nombreNuevo = string(nombreArchivo),
     linea = " ",
      actual,
       nombreSalida = "RESULTADO.txt";
    int exitosos = 0, fallidos = 0;
    //Abre el archivo con las hileras de prueba y los ejecuta en el automataº
    if(manipulaArchivos.abreArchivo(nombreArchivo, true)){
        if(!manipulaArchivos.abreArchivo(nombreSalida, false)){
            return false;
        }
        while(!manipulaArchivos.lector.eof()){
            manipulaArchivos.lector >> linea;        
            viajaPorAutomata(linea, 0);
            //Si el recorrido fue fallido, lo escribe en el archivo de salida
            if(obtieneFallidos() >= 1 && obtieneExitosos() == 0){
                manipulaArchivos.escritor << linea << " - Fallido" << endl;
                fallidos++;
            }
            //Si el recorrido fue exitoso, lo escribe en el archivo de salida
            //else if(obtieneExitosos() >= 1 && obtieneFallidos()>1){
            else{    
                manipulaArchivos.escritor << linea << " -  Exitoso" << endl;
                exitosos++;
            }

            reiniciaExitosos();
            reiniciaFallidos();
        }
        manipulaArchivos.escritor.close();
        manipulaArchivos.lector.close();
        return true;
    }
    return false;    
}

void Automata::viajaPorAutomata(char deseado, int idEstadoActual, vector<int> * destinos){

        Estado * existe = buscaEnLista(idEstadoActual);
        if(existe->obtieneID()==-1){
            this->incrementaFallidos();
            return;
        }

        vector<int> resultados = existe->buscaPosiblesDestinos(deseado);
        vector<int>::iterator iterador;
        int nombreEstadoEncontrado;
        Estado * estadoEncontrado = new Estado();
        if(resultados.empty()){
            this->incrementaFallidos();
            return;
        }
        for(iterador=resultados.begin();
        iterador!=resultados.end();
        iterador++){
            nombreEstadoEncontrado = *iterador;
            estadoEncontrado = buscaEnLista(nombreEstadoEncontrado);
            if(estadoEncontrado->obtieneID()!=-1){
                if(!contieneLista(nombreEstadoEncontrado, destinos)){
                    destinos->push_back(estadoEncontrado->obtieneID());
                    viajaPorAutomata(deseado, estadoEncontrado->obtieneID(), destinos);
                }
            }
        }
    
}

void Automata::asignaExitosos(int exitosos){
    this->exitosos = exitosos;
}

void Automata::asignaFallidos(int fallidos){
    this->fallidos = fallidos;
}

int Automata::obtieneExitosos(){
    return this->exitosos;
}

int Automata::obtieneFallidos(){
    return this->fallidos;
}

void Automata::incrementaExitosos(){
    this->exitosos++;
}

void Automata::incrementaFallidos(){
    this->fallidos++;
}

void Automata::reiniciaExitosos(){
    this->exitosos = 0;
}

void Automata::reiniciaFallidos(){
    this->fallidos = 0;
}

void Automata::asignaVector(vector<Estado*> * vector){
    this->todosEstados = vector;
}

void Automata::imprimeEstadosConTransiciones(){
    vector<Estado*>::iterator iterador;
    for(iterador = this->todosEstados->begin();
    iterador != this->todosEstados->end();
    iterador++){
        Estado * estado = *iterador;
        cout << estado->obtieneID() << ":" << endl;
        estado->imprimeTransiciones();
    }
}

void Automata::imprimeEstadosFinales(){
    vector<Estado*>::iterator iterador;
    for(iterador=this->todosEstados->begin();
    iterador!=this->todosEstados->end();
    iterador++){
        Estado * temp = *iterador;
        if(temp->obtieneEsFinal()){
            cout << temp->obtieneID() << endl;
        }
    }
}

Automata::~Automata(){}