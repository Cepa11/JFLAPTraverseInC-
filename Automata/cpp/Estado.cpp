#include "../Header/Estado.h"

Estado::Estado(){
    this->id = -1;
    this->esFinal = false;
    this->transiciones = new vector<Transicion*>();
}

Estado::Estado(int id){
    this->id = id;
    this->transiciones = new vector<Transicion*>();
}

Estado::Estado(int id, bool esFinal){
    this->id = id;
    this->esFinal = esFinal;
    this->transiciones = new vector<Transicion*>();
}

void Estado::asginaID(int id){
    this->id = id;
}

void Estado::asignaEsFinal(bool esFinal){
    this->esFinal = esFinal;
}

int Estado::obtieneID(){
    return this->id;
}

bool Estado::obtieneEsFinal(){
    return this->esFinal;
}

void Estado::agregaTransicion(int destino, string valores){
    Transicion * temp= new Transicion();
    temp->asignaDestino(destino);
    temp->asignaPosibleValor(valores);
    transiciones->push_back(temp);
}

void Estado::agregaTransicion(Transicion * transicion){
    transiciones->push_back(transicion);
}

vector<int> Estado::buscaPosiblesDestinos(char valor){
    Transicion * temp = new Transicion();
    vector<Transicion*>::iterator iterador;
    vector<int> resultado;

    for(iterador = transiciones->begin();
    iterador != transiciones->end();
    iterador++){
        temp = *iterador;
        if(temp->obtienePosibleValor().find(valor) != string::npos){
            resultado.push_back(temp->obtieneDestino());
        }
    }
    return resultado;
}

void Estado::imprimeTransiciones(){
    vector<Transicion*>::iterator iterador;
    for(iterador = this->transiciones->begin();
    iterador != this->transiciones->end();
    iterador++){
        Transicion * transicion = *iterador;
        cout << " " << transicion->obtieneDestino() << ", " << transicion->obtienePosibleValor() << endl;
    }
}
Estado::~Estado(){}