#include "../Header/Transicion.h"
Transicion::Transicion(){
    this->posibleValor ="N/A";
    this->destino = -1;
}

Transicion::Transicion(int destino, string posibleValor){
    this->destino = destino;
    this->posibleValor = posibleValor;
}

void Transicion::asignaDestino(int destino){
    this->destino = destino;
}

void Transicion::asignaPosibleValor(string posibleValor){
    this->posibleValor = posibleValor;
}
string Transicion::obtienePosibleValor(){
    return this->posibleValor;
}

int Transicion::obtieneDestino(){
    return this->destino;
}

Transicion::~Transicion(){}