#include "../Header/Automata.h"
#include "../Header/ManipulaArchivos.h"

using namespace std;

void error(){
    cout << "Por favor incluya el nombre del archivo que desea utilizar\nPor ejemplo \"RecorreAutomata.exe  \"NombreDelArchivo.txt\"" << endl;
}

int main(int argc, char* argv[]){
   if(argc !=3){
       cout << argc << endl;
       error();
       return EXIT_FAILURE;
    }
   cout << "Intentando recorrer con el archivo " << argv[2] << " con el automata del archivo " << argv[1] << endl;
   string nombreArchivo=argv[1];
   bool pudoRecorrer = false;
   ManipulaArchivos manipulaArchivos;
   Automata automata;
   vector<Estado*> * automataDesdeArchivo = NULL;
    automataDesdeArchivo = manipulaArchivos.parseaXMLPorNombresNodos(nombreArchivo);
   if(automataDesdeArchivo == NULL){
       cout << "Error al cargar el automata desde el archivo, cerrando el programa" << endl;
       free(automataDesdeArchivo);
       return EXIT_FAILURE;
   }
   automata.asignaVector(manipulaArchivos.parseaXMLPorNombresNodos(nombreArchivo));
   pudoRecorrer = automata.leeYRecorreAutomata(manipulaArchivos, argv[2]);
   if(!pudoRecorrer){
       cout << "Error al leer o escribir el archivo " << argv[2] << " y al recorrer el automata" << endl;
       free(automataDesdeArchivo);
       return EXIT_FAILURE;
   }
   cout << "El archivo resultado se encuentra en la carpeta actual, con el nombre RESULTADO.txt" << endl;
   free(automataDesdeArchivo);
   return EXIT_SUCCESS;
}

